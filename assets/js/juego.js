/*
    2C = Two of Clubs
    2D = Two of Diamonds
    2H = Two of Hearts
    2S = Two of Spades
*/
(() => {

    'use strict'
    
    let deck = [];

    const tipos = ['C', 'D', 'H', 'S'];
    const especiales = ['A', 'J', 'Q', 'K'];

    let puntosJugador = 0,
        puntosComputadora = 0;

    // Referncias del html
    const btnPedir = document.querySelector('#btnPedir');
    const btnDetener = document.querySelector('#btnDetener');
    const btnNuevo = document.querySelector('#btnNuevo');
    const puntosHTML = document.querySelectorAll('small');
    const divCartasJugador = document.querySelector('#jugador-cartas');
    const divCartasComputadora = document.querySelector('#computadora-cartas');

    console.log(btnPedir);



    const crearDeck = () => {

        for (let i = 2; i <= 10; i++) {
            for( let tipo of tipos) {
                deck.push( i + tipo );
            }
        }
        
        for (let tipo of tipos) {
            for (let esp of especiales) {
                deck.push( esp + tipo);
            }
        }


        
        // console.log( deck );

        deck = _.shuffle(deck);
        // console.log(deck);

        return deck;
    }


    const pedirCarta = () => {

        if (deck.length === 0 ) {
            throw 'No hay cartas en el deck'
        }

        const carta = deck.pop();

        // console.info({ carta });
        
        return carta;
        
    };

    const valorCarta = (carta) => {
        
        const valor = carta.substring(0, carta.length - 1);
        // let puntos = 0;
        // if (isNaN(valor)) {
        //     console.log('no numero');
        //     puntos = ( valor === 'A') ? 11 : 10;
        // } else {
        //     console.log('numero');
        //     puntos = valor * 1;
        // }
        
        // console.log(puntos);

        return ( isNaN( valor ) ) ?
                ( valor === 'A') ? 11 : 10
                : valor * 1;
    }

    const turnoComputadora = ( puntosMinimo ) => {

        do {
            const carta = pedirCarta();

            // console.log({carta});
            puntosComputadora = puntosComputadora + valorCarta( carta );
            puntosHTML[1].innerText = puntosComputadora;

            const imgCarta = document.createElement('img');
            // imgCarta.src = `assets/cartas/$ { carta } .png`;
            imgCarta.src = `assets/cartas/${ carta }.png`;
            imgCarta.classList.add('carta');
            
            divCartasComputadora.append( imgCarta );

            if ( puntosMinimo > 21 ) {
                break;

            }
        }
        while( (puntosComputadora < puntosMinimo) && (puntosMinimo <= 21 ) );

    }

    crearDeck();
    // console.log({ deck });
    const valor = valorCarta( pedirCarta() );
    // console.log( valor );


    //Eventos

    btnPedir.addEventListener('click', function() {
        console.log('click');

        const carta = pedirCarta();

        console.log({carta});
        puntosJugador = puntosJugador + valorCarta( carta );

        puntosHTML[0].innerText = puntosJugador;

        

        const imgCarta = document.createElement('img');
        // imgCarta.src = `assets/cartas/$ { carta } .png`;
        imgCarta.src = `assets/cartas/${ carta }.png`;
        imgCarta.classList.add('carta');
        
        divCartasJugador.append( imgCarta );
        console.log(puntosJugador);

        if (puntosJugador > 21) {
            console.warn('Lo siento mucho, perdiste');  
            btnPedir.disabled = true;
            btnPedir.disabled = false;
            btnDetener.disabled = false;

            turnoComputadora( puntosJugador );
            
        } else if (puntosJugador === 21 ) {
            console.log('21, genial');
            turnoComputadora( puntosJugador );

        }

    });

    btnDetener.addEventListener('click', () => {
        btnPedir.disabled = false;
        btnDetener.disabled = false;

        turnoComputadora(puntosJugador);

    });


    btnNuevo.addEventListener('click', () => {
        
    });

    // console.log( 16 );
    // turnoComputadora( 16 );

})();


